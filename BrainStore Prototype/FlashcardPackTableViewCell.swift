//
//  FlashcardPackTableViewCell.swift
//  BrainStore Prototype
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import UIKit

// protocol used for information when the cell's switch is pressed
protocol SwitchInfoDelegate: class {
    func sendSwitchInfo(state: Bool, cell: UITableViewCell)
}

class FlashcardPackTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var enabledSwitch: UISwitch!
    
    // making this a weak variable so that it won't create a strong reference cycle
    weak var delegate: SwitchInfoDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Triggered when the cell's switch is pressed. Sends info to FlashcardTableViewController to be handled.
    @IBAction func switchPressed(_ sender: Any) {
        delegate?.sendSwitchInfo(state: enabledSwitch.isOn, cell: self)
    }
    
}
