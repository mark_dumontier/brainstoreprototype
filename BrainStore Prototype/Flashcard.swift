//
//  Flashcard.swift
//  BrainStore Prototype
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import Foundation

struct Flashcard: Codable {
    
    //MARK: Properties
    
    let term: String
    let definition: String
    var enabled: Bool
    
    //MARK: Initialization
    
    init?(term: String, definition: String, enabled: Bool){
        
        //Initialization should fail if there is no term or definition
        
        if term.isEmpty || definition.isEmpty {
            return nil
        }
        
        self.term = term
        self.definition = definition
        self.enabled = enabled
    }
    
    init?(term: String, definition: String){
        
        //Initialization should fail if there is no term or definition
        
        if term.isEmpty || definition.isEmpty {
            return nil
        }
        
        self.term = term
        self.definition = definition
        self.enabled = true
    }
 /*
    //MARK: Methods
    
    mutating func enable() {
        enabled = true
    }
    
    mutating func disable() {
        enabled = false
    }
 */
}
