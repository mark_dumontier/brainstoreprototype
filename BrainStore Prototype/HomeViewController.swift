//
//  ViewController.swift
//  BrainStore Prototype
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import UIKit
import UserNotifications
import Crashlytics

class HomeViewController: UIViewController, UNUserNotificationCenterDelegate, PackInfoDelegate {

    //MARK: Properties
    var packs = [FlashcardPack]()
    var testArray = [Flashcard]()
    var notificationDelay = TimeInterval(120)
    var notificationsOn = false
    
    @IBOutlet weak var delayPickerOutlet: UIDatePicker!
    @IBOutlet weak var startButtonOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadSamplePacks()
        
        //Set the delay picker to a default value
        delayPickerOutlet.countDownDuration = notificationDelay
    }

    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPackList" {
            // get a reference to the second view controller
            let secondViewController = segue.destination as! FlashcardPackTableViewController
            
            // Pass pack information to the second view controller
            secondViewController.packs = packs
            
            // Set this view controller as the delegate so that the pack information can be sent back
            secondViewController.delegate = self
        }

    }
    
    //Delegate method to receive flashcard pack info sent back by FlashCardPackTableViewController
    func sendPackInfo(packs: [FlashcardPack]) {
        self.packs = packs
    }
    
    //MARK: Notification Control
    
    func registerCategories(){
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let category = UNNotificationCategory(identifier: "flashcard",
                                              actions: [],
                                              intentIdentifiers: [],
                                              options: .customDismissAction)
        
        center.setNotificationCategories([category])
    }
    
    //Handle the response given to a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        
        let userInfo = response.notification.request.content.userInfo
        
        if let customData = userInfo["customData"] as? String {
            print(customData)
            
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                stopNotifications()
                
            case UNNotificationDismissActionIdentifier:
                print("Notification dismissed")
                scheduleLocal()
                
            default:
                break
            }
        }
        completionHandler()
    }
    
    //Handle a notification when the app is in the foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void ) {
        
        // show alert while app is running in foreground
        return completionHandler(UNNotificationPresentationOptions.alert)
        
    }
    
    //MARK: Actions
    
    @IBAction func toggleNotifications(_ sender: UIButton) {
        if notificationsOn{
            stopNotifications()
        } else{
            startButtonOutlet.setTitle("Stop Notifications", for: .normal)
            notificationsOn = true
            fillTestArray()
            scheduleLocal()
        }

    }
    
    //Clears the notification queue and resets the start notifications button
    func stopNotifications() {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        notificationsOn = false
        startButtonOutlet.setTitle("Begin Notifications", for: .normal)
        print("Notifications stopped")
    }
    
    //Creates an array of flashcards from the set of flashcard packs that are enabled
    func fillTestArray(){
        
        //Clear the testArray of any previously added cards
        testArray.removeAll()
        
        //Add flashcards from enabled packs to testArray
        for pack in packs {
            if pack.enabled {
                testArray += pack.flashcards
            }
        }
        
        //TODO: Handle repeat terms from different flashcard packs
        
    }
    
    
    @objc func scheduleLocal(){
        
        registerCategories()
        let center = UNUserNotificationCenter.current()
        
        //Remove all pending requests
        center.removeAllPendingNotificationRequests()
        
        //Retrieve a random flashcard from testArray
        guard let testCard = testArray.randomElement() else {
            
            //If the test array is empty, there is nothing to fill notifications with. Cancel notifications.
            print("No flashcards enabled")
            
            stopNotifications()
            
            //Show an alert letting the user know they have no flashcards enabled.
            let alert = UIAlertController(title: "No flashcards are enabled.", message: "You must enable at least one flashcard pack before beginning notifications.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
            return
        }
        
        let content = UNMutableNotificationContent()
        content.title = testCard.term
        content.body = testCard.definition
        content.categoryIdentifier = "flashcard"
        content.userInfo = ["customData": "flashcardResponse"]
        content.sound = UNNotificationSound.default
        
        notificationDelay = delayPickerOutlet.countDownDuration
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notificationDelay, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString,
                                            content: content,
                                            trigger: trigger)
        
        center.add(request)
        
        print("Notification scheduled with a delay of \(notificationDelay) seconds")
        
    }
    
    //MARK: Private Methods
    
    private func loadSamplePacks() {
        
        //Sample flashcard pack definitions
        let flashcards1 = [
            Flashcard(term: "Acceleration", definition: "The rate of change of the velocity of an object with respect to time.")!,
            Flashcard(term: "Ampere", definition: "The base unit of electric current")!,
            Flashcard(term: "Factor of Safety", definition: "A term describing the load-carrying capability of a system beyond the expected or actual loads.")!
        ]
        let flashcards2 = [
            Flashcard(term: "A", definition: "Alpha")!,
            Flashcard(term: "B", definition: "Bravo")!,
            Flashcard(term: "C", definition: "Charlie")!,
            Flashcard(term: "D", definition: "Delta")!
        ]
        let flashcards3 = [
            Flashcard(term: "Scalene Triangle", definition: "a triangle having three unequal sides and angles")!,
            Flashcard(term: "Vertex", definition: "the intersection point of two sides of a plane figure")!,
            Flashcard(term: "Right Triangle", definition: "a triangle with one internal angle equal to 90 degrees")!,
            Flashcard(term: "Pentagon", definition: "a polygon with 5 sides and 5 angles")!
        ]
        
        let engineering = FlashcardPack(name: "Engineering Terms", description: "A set of engineering terms.", flashcards: flashcards1, enabled: true)!
        let natoAlphabet = FlashcardPack(name: "NATO Alphabet", description: "Letters and their NATO alphabet equivalent.", flashcards: flashcards2, enabled: false)!
        let geometry = FlashcardPack(name: "Geometry Terms", description: "A set of geometry terms.", flashcards: flashcards3, enabled: true)!
        
        packs += [engineering, natoAlphabet, geometry]
    }
    
    
}

