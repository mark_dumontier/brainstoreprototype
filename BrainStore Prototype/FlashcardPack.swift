//
//  FlashcardPack.swift
//  BrainStore Prototype
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import Foundation

struct FlashcardPack: Codable {
    
    //MARK: Properties
    
    let name: String
    let description: String
    var flashcards: [Flashcard]
    var enabled: Bool
    

    
    //MARK: Initialization

    init?(name: String, description: String, flashcards: [Flashcard], enabled: Bool){
        
        if name.isEmpty || description.isEmpty {
            return nil
        }
        
        self.name = name
        self.description = description
        self.flashcards = flashcards
        self.enabled = enabled
    }

    init?(name: String, description: String){
        
        if name.isEmpty || description.isEmpty {
            return nil
        }
        
        self.name = name
        self.description = description
        self.flashcards = []
        self.enabled = true
    }
    
    //MARK: Methods
    
    mutating func enable() {
        enabled = true
    }
    
    mutating func disable() {
        enabled = false
    }
    
}
