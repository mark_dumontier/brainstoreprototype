//
//  FlashcardPackTableViewController.swift
//  BrainStore Prototype
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import UIKit

// protocol used for sending pack information back
protocol PackInfoDelegate: class {
    func sendPackInfo(packs: [FlashcardPack])
}


class FlashcardPackTableViewController: UITableViewController, SwitchInfoDelegate {

    

    //MARK: Properties
    
    var packs = [FlashcardPack]()
    
    // making this a weak variable so that it won't create a strong reference cycle
    weak var delegate: PackInfoDelegate? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return packs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        // Table view cells are reused and should be dequeued using a cell identifier
        let cellIdentifier = "FlashcardPackTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FlashcardPackTableViewCell  else {
            fatalError("The dequeued cell is not an instance of FlashcardPackTableViewCell.")
        }
        
        let pack = packs[indexPath.row]
        
        cell.nameLabel.text = pack.name
        cell.descriptionLabel.text = pack.description
        
        if pack.enabled {
            cell.enabledSwitch.setOn(true, animated: true)
        } else{
            cell.enabledSwitch.setOn(false, animated: true)
        }
        
        cell.delegate = self
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation
    
    //This function is called when the back button is pressed
    override func willMove(toParent parent: UIViewController?){
        super.willMove(toParent: parent)
         if parent == nil
         {
            //Send updated pack info to the HomeViewController
            delegate?.sendPackInfo(packs: packs)
         }
     }
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: Actions
    
    //Triggered when a switch in a table cell is pressed. Toggles the enabled state of the cell's flashcard pack
    func sendSwitchInfo(state: Bool, cell: UITableViewCell ) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            print("failed to get index path for cell containing switch")
            return
        }
        packs[indexPath.row].enabled = state
        print("\(packs[indexPath.row].name) is now \(state)")
    }
    
    /*
    //Use this method to determine when a row is selected.
    override func tableView(_ tableView:UITableView, didSelectRowAt indexPath: IndexPath){
        
        let pack = packs[indexPath.row]
        
        print(pack.name)
     
        /*
        //Use to deselect a row after letting go
        tableView.deselectRow(at: indexPath, animated: true)
        */
     
    }
    */
    
    

}
