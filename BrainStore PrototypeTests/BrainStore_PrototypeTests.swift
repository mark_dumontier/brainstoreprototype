//
//  BrainStore_PrototypeTests.swift
//  BrainStore PrototypeTests
//
//  Created by CylineaSoft Developer Account on 2018-09-28.
//  Copyright © 2018 CylineaSoft Developer Account. All rights reserved.
//

import XCTest
@testable import BrainStore_Prototype

class BrainStore_PrototypeTests: XCTestCase {

    //MARK: Flashcard Structure Tests
    
    // Confirm that the Flashcard initializer returns a Flashcard object when passed valid parameters.
    func testFlashcardInitializationSucceeds(){
        
        let flashcard = Flashcard.init(term: "term", definition: "definition")
        XCTAssertNotNil(flashcard)
        
        let flashcardOptionalInit = Flashcard.init(term: "term", definition: "definition", enabled: false)
        XCTAssertNotNil(flashcardOptionalInit)
    }

    // Confirm that the Flashcard initializer returns nil when passed a empty term or an empty definition
    func testFlashcardInitializationFails(){
        
        let emptyTermFlashcard = Flashcard.init(term: "", definition: "definition")
        XCTAssertNil(emptyTermFlashcard)
        
        let emptyTermFlashcardOptionalInit = Flashcard.init(term: "", definition: "definition", enabled: false)
        XCTAssertNil(emptyTermFlashcardOptionalInit)
        
        let emptyDefinitionFlashcard = Flashcard.init(term: "", definition: "definition")
        XCTAssertNil(emptyDefinitionFlashcard)
        
        let emptyDefinitionFlashcardOptionalInit = Flashcard.init(term: "", definition: "definition", enabled: false)
        XCTAssertNil(emptyDefinitionFlashcardOptionalInit)
    }
    
    // Confirm that Flashcard can be stored with Codable
    func testFlashcardCodingSucceeds(){
        
        let flashcard = Flashcard.init(term: "term", definition: "definition", enabled: true)
        let encodedData = try? JSONEncoder().encode(flashcard)
        XCTAssertNotNil(encodedData)
        guard let dataToDecode = encodedData else {
            XCTFail()
            return
        }
        
        let decodedData = try? JSONDecoder().decode(Flashcard.self, from: dataToDecode)
        XCTAssertNotNil(decodedData)
    }
    
    
    //MARK: FlashcardPack Tests
    
    // Confirm that the FlashcardPack initializer returns a FlashcardPack object when passed valid parameters.
    func testFlashcardPackInitializationSucceeds(){
        
        let flashcardPack = FlashcardPack.init(name: "name", description: "description", flashcards: [Flashcard](), enabled: true)
        XCTAssertNotNil(flashcardPack)
        
        let flashcardPackOptionalInit = FlashcardPack.init(name: "name", description: "description")
        XCTAssertNotNil(flashcardPackOptionalInit)
    }
    
    // Confirm that the FlashcardPack initializer returns nil when passed an empty name or an empty description
    func testFlashcardPackInitializationFails(){
        
        let emptyNameFlashcardPack = FlashcardPack.init(name: "", description: "description", flashcards: [], enabled: true)
        XCTAssertNil(emptyNameFlashcardPack)
        
        let emptyNameFlashcardPackOptionalInit = FlashcardPack.init(name: "", description: "description")
        XCTAssertNil(emptyNameFlashcardPackOptionalInit)
        
        let emptyDescriptionFlashcardPack = FlashcardPack.init(name: "name", description: "", flashcards: [], enabled: true)
        XCTAssertNil(emptyDescriptionFlashcardPack)
        
        let emptyDescriptionFlashcardPackOptionalInit = FlashcardPack.init(name: "name", description: "")
        XCTAssertNil(emptyDescriptionFlashcardPackOptionalInit)
    }
    
    // Confirm that FlashcardPack can be stored with Codable
    func testFlashcardPackCodingSucceeds(){
        
        let flashcardPack = FlashcardPack.init(name: "name", description: "description", flashcards: [Flashcard](), enabled: true)
        let encodedData = try? JSONEncoder().encode(flashcardPack)
        XCTAssertNotNil(encodedData)
        guard let dataToDecode = encodedData else {
            XCTFail()
            return
        }
        let decodedData = try? JSONDecoder().decode(FlashcardPack.self, from: dataToDecode)
        XCTAssertNotNil(decodedData)
    }
    
    
}
